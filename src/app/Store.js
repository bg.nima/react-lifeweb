
import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk"; 
import { AppReducer, LifeWebReducer } from "./reducers";

export function configureStore(initialState)  {
	return createStore(combineReducers({
												app: AppReducer,
												lifeweb: LifeWebReducer
											}),
										 initialState,
										 applyMiddleware(thunk));
}