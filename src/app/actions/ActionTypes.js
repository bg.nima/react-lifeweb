
let ActionTypes = {
	LOAD_NEWS_CLOUD : 'LOAD_NEWS_CLOUD',
	LOAD_CURRENT_USER_INFO: 'LOAD_CURRENT_USER_INFO'
}

export default ActionTypes;