import React from "react";
import { Navbar, Sidebar, MainContainer, Footer } from "./";
import { connect } from "react-redux";


class App extends React.Component {

	render() {
		let {navbarTitle} = this.props;
		return (
			<div className="wrapper">
				<Sidebar />
				<div className="main-panel">
					<Navbar title={navbarTitle} />
					<MainContainer />
					<Footer />
				</div>
			</div>
		);
	}

}

function mapStateToProps(state) {
	return {
		navbarTitle:  state.app.navbarTitle
	}
}

export default connect(mapStateToProps)(App);