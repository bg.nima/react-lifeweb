import React from 'react';
import { connect } from 'react-redux';


import { setNavbarTitle } from '../../actions/AppActions';
import DashboardPage from '../../components/pages/dashboard/DashboardPage';

class MainContainerComponent extends React.Component {

	componentWillMount() {
		this.props.setNavbarTitle('صفحه داشبورد');
	}

	render() {
		return (
			<div className="content">		
					{/* Routings must be placed here */}
					<DashboardPage />
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {}
}

function mapDispatchToProps(dispatch) {
	return {
		setNavbarTitle: (title) => dispatch(setNavbarTitle(title))
	}
}

let MainContainer = connect(mapStateToProps, mapDispatchToProps)(MainContainerComponent)

export { MainContainer }