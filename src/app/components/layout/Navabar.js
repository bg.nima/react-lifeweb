import React from "react";
import PropTypes from "prop-types";

// import "../../assets/img/logo.png"

class Navbar extends React.Component {
	render() {
		var { title } = this.props;
		return (
			<nav className="navbar navbar-default navbar-fixed">
				<div className="container-fluid">
					<div className="navbar-header">
						<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span className="sr-only">Toggle navigation</span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
						</button>
						<a className="navbar-brand" href="#">{title}</a>
					</div>
					<div className="collapse navbar-collapse">

						<ul className="nav navbar-nav navbar-right">
							<li>
								<a href="#">
									<p>خروج</p>
								</a>
							</li>
							<li className="separator hidden-lg"></li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

Navbar.propTypes = {
	title: PropTypes.string.isRequired
}

export { Navbar };