import React from 'react';
import NewsCloud from './NewsCloud';
import UserWelcome from './UserWelcome';

export default class DashboardPage extends React.Component {

	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-8">
						<NewsCloud />
					</div>			
					<div className="col-md-4">
						<UserWelcome />
					</div>
				</div>
			</div>
		)
	}

}