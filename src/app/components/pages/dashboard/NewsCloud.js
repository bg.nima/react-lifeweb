import React from 'react';
import jqcloud2 from 'jqcloud2';
import {connect } from 'react-redux';
import $ from 'jquery';

import * as LifeWebActions from '../../../actions/LifeWebActions'
import { Card, Progress } from '../../common';

import '../../../../../node_modules/jqcloud2/dist/jqcloud.min.css';
import './NewsCloud.scss';


class NewsCloud extends React.Component {

	constructor(props) {
		super(props);
		this.coludContainer = null;
	}

	componentDidMount() {
		this.props.loadNewsCloud();
		$(this.coludContainer).jQCloud([]);
	}

	componentWillUpdate(props, state) {
		$(this.coludContainer).jQCloud('update', props.newsCloud);
	}

	render () {
		return (
		<Card header='ابر خبر' subHeader='گستردگی کلمات کلیدی اخبار در ۲۴ ساعت اخیر'>
			<div style={{height: '42vh'}} ref={(elm) => this.coludContainer = elm}>
				{this.props.newsCloud.length ? null : <Progress /> }
			</div>
		</Card>
		)
	}
}

function mapStateToProps(state) {
	return {
		newsCloud: state.lifeweb.newsCloud
	}
}

function mapDispatchToProps(dispatch) {
	return {
		loadNewsCloud: () => dispatch(LifeWebActions.loadNewsCloudAsync())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsCloud);