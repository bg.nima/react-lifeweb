import React from "react";
import { connect } from "react-redux";

import { Card, Progress } from "../../common";
import * as LifeWebActions from '../../../actions/LifeWebActions'

class UserWelcome extends React.Component {

	componentDidMount() {
		this.props.loadCurrentUserInfo();
	}

	render () {
		let userInfo = this.props.userInfo;
		console.log(userInfo)
		return (
			<Card header='خوش آمد گویی'>
				<div style={{height:'45vh'}}>
					{userInfo ? this.renderUserInfo(userInfo) : <Progress /> }
				</div>
			</Card>
		);
	}

	renderUserInfo(userInfo) {
		return (
			<div>
				<h3 className="text-center"> سلام {userInfo.name} </h3>
				<h4 className="text-center">{userInfo.email}</h4>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		userInfo: state.lifeweb.userInfo
	};
}

function mapDispatchToProps(dispatch) {
	return { 
		loadCurrentUserInfo: () => dispatch(LifeWebActions.loadCurrentUserInfoAsync())
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UserWelcome);