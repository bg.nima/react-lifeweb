
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import "bootstrap"; 
// import { AppContainer } from "react-hot-loader";

import "./app/components/layout/LightBootstrapDashboard";
import * as Store from './app/Store';
import App from './app/components/layout/App'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './assets/style/style.scss';
import "./assets/img/logo.png";

var store = Store.configureStore();

var Base = (
	<div>
		<Provider store={store}>
			<App/>
		</Provider>
	</div>
)

ReactDOM.render(Base, document.getElementById('app'));